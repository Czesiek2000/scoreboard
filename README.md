<div align="center"><h1>SCOREBOARD</h1></div>
This is scoreboard that was inspired from fivem scoreboard resources. It is fully customizable. It displays real time, player: id, name, socialclubid and ping, also displays number of all players that can play on the server and number of players that are currently in the game.

## Preview
You see here see preview from the game.
![preview](./preview.png)

[Here](https://czesiek2000.gitlab.io/scoreboard/) you can also find real example but in the browser.
If you head up to this link you will find scoreboard without rows. You can add them by pressing `F12` and redirect to console. There you can write sample code to add data to table rows.

```js
insert(1, "Test", 1234, 100, 100); // insert one row

insert(1, "Test", Math.floor(Math.random() * 1000), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100)); // insert row with random data

for(let i = 0; i < 5; i++){
    insert(Math.floor(Math.random() * 10), "Test", Math.floor(Math.random() * 1000), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100)); // insert 5 rows with random data, to add more change 5 to number of values you want to add
}

```

## Instalation
Place all files in their proper directory.
* Copy **scoreboard** folder from this **client_packages** folder and place it inside your server files **client_packages** folder, which can be found inside your server files folder in `server-files/client_packages` folder.
* Navigate to the root of **client_packages** folder and inside index.js add following line
```js
require('scoreboard');
```
* Then copy **scoreboard** folder from **packages** folder and place it inside your server files packages folder, which you can find inside your server files folder in `server-files/packages` folder.
* Navigate to the root of **packages** folder and inside `index.js` add following line
```js
require('scoreboard')
```

## Configuration
This resource is fully customizable. If you have some coding skills you can try to customize design or displayed values. 

If you don't have coding skills and want to change something in this repository you should find `script.js` file, which you can find inside `client_packages/scoreboard/ui/script.js`. 
There you will find in the first lines config object which should look something like this: 

```js
const config = {
    title: 'Server Title', // replace Server Title text
    list: 'Players list', // replace text under Server Title
    time: 'Current time', // Prefix that will display before time
    image: 'background.jpg', // path to background, if you don't want just leave ''
        th: {
            id: 'Id', // first value in the green table header
            name: 'Name', // second value in the green table header
            socialId: 'Social club Id', // third value in the green table header
            ping: 'Ping', // fourth value in the green table header
        }
}

```

If you :thumbsup: or :heart: this resource, don't forget to leave :star: on this repo or :pencil: nice comment on the [forum](https://rage.mp/forums/).

If you found any :bug: in this script, leave :thought_balloon: on the [forum](https://rage.mp/forums/) or open [issue](https://gitlab.com/Czesiek2000/notifications/-/issues) in this repo. 
Thanks :punch:, enjoy this resource.
