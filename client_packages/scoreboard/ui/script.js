const config = {
    title: 'Server Title',
    list: 'Players list',
    time: 'Current time',
    image: 'background.jpg',
        th: {
            id: 'Id',
            name: 'Name',
            socialId: 'Social club Id',
            ping: 'Ping',
        }
}

document.querySelector('.title').innerText = config.title;
document.querySelector('.listTitle').innerText = config.list;

if (config.image === '') {
    document.querySelector('img').remove();
}else {
    document.querySelector('.background').src = config.image;
}

document.querySelectorAll('th')[0].innerText = config.th.id;
document.querySelectorAll('th')[1].innerText = config.th.name;
document.querySelectorAll('th')[2].innerText = config.th.socialId;
document.querySelectorAll('th')[3].innerText = config.th.ping;

function insert(id, name, scId, ping) {
    document.querySelector('.table tbody').innerHTML += `<tr><td>${id}</td><td>${name}</td><td>${scId}</td><td>${ping}</td></tr>`    
    // if (document.querySelectorAll('tr') !== undefined) {
        let arr = [...document.querySelectorAll('tr td')];
        arr.shift();
        arr.forEach(color => {
            color.style.color = `#${Math.floor(Math.random()*16777215).toString(16)}`
        })
    // }
}

document.addEventListener('keydown', (e) => {
    if (e.key === 'z') {
        if (document.querySelector('.main').style.display === 'none') {
            document.querySelector('.main').style.display = 'block';
        }else {
            document.querySelector('.main').style.display = 'none';
        }
    }
})

function addZero(date){
    return date < 10 ? `0${date}` : date;
}

setInterval(() => {
    let date = new Date();
    document.getElementById('time').innerText = `${config.time}: ${addZero(date.getHours())} : ${addZero(date.getMinutes())} : ${addZero(date.getSeconds())}`
}, 500)

function max(max) {
    document.getElementById('max-players').innerText = max;
}

function local(max) {
    document.getElementById('player-count').innerText = max;
}