let browser;
mp.events.add('guiReady', () => {
    if (!browser) {
        browser = mp.browsers.new('package://scoreboard/ui/index.html');
    }
})

mp.events.add('addPlayers', (id, _name, scId, ping) => {
    mp.game.graphics.notify('addplayers')
    browser.execute(`insert('${id}', '${_name}', '${scId}', '${ping}')`)
})

mp.events.add('maxPlayers', (max) => {
    mp.game.graphics.notify('add max')
    browser.execute(`max('${max}')`);
})

mp.events.add('localPlayers', (value) => {
    mp.game.graphics.notify('add locals');
    browser.execute(`local('${value}')`);
})